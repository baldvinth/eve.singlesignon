# Welcome

This documentation is very much a work in progress, but to get people started here's an example for a SSO controller.

```csharp
using EVE.SingleSignOn;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TestApplication.Controllers
{
    public class SSOController : Controller
    {
        private readonly string _returnKey = "TestApplication.Session.ReturnUrl";
        private ISingleSignOnClient _sso;

        public SSOController()
        {
            _sso = new SingleSignOnClient();
        }

        /// <summary>
        /// Redirect the user to the CCP login page, 
        /// assuming they're not already logged in.
        /// </summary>
        /// <param name="r">Absolute path for the redirect</param>
        /// <returns></returns>
        public ActionResult Login(string r)
        {
            // If a return path has been provided, and it's a local path
            // we create a session state to return them upon callback
            if (!string.IsNullOrEmpty(r) && Url.IsLocalUrl(r))
                Session[_returnKey] = r;
            else
                Session[_returnKey] = null;

            // User is logged in, he has no business trying to log back in
            if (_sso.IsLoggedIn())
            {
                // If a return path has been provided, throw them there
                if (Session[_returnKey] != null)
                    return Redirect(Session[_returnKey] as string);

                return RedirectToAction("Index", "Home");
            }

            // Send the user to the SSO login
            return Redirect(_sso.GetAuthenticationUrl());
        }

        /// <summary>
        /// Log a user out from the system
        /// </summary>
        /// <param name="r">Absolute path for the redirect</param>
        /// <returns></returns>
        public ActionResult Logout(string r)
        {
            // If the user is logged in, kill the session
            if (_sso.IsLoggedIn())
            {
                Session.Clear();
                Session.Abandon();
            }

            if (!string.IsNullOrEmpty(r) && Url.IsLocalUrl(r))
                return Redirect(r);
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Handle the callback from EVE Online's SSO
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task<ActionResult> Callback(string code, string state)
        {
            // Authenticate the code we received from the SSO
            SsoResponse authentication = await _sso.AuthenticateAsync(code);

            // Retrieve the character information from the SSO
            SsoCharacter character = await _sso.VerifyAsync(authentication.AccessToken);

            if (Session[_returnKey] != null)
                return Redirect(Session[_returnKey] as string);
            else
                return RedirectToAction("Index", "Home");
        }
    }
}
```
