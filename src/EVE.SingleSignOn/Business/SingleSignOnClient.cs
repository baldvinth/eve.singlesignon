﻿using EVE.SingleSignOn.Utils;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EVE.SingleSignOn
{
    public class SingleSignOnClient : ISingleSignOnClient
    {
        private readonly string _characterKey = "EVE.SingleSignOn.Session.LoggedInCharacter";
        private readonly string _userAgent;
        private readonly List<HttpStatusCode> _statusCodes = new List<HttpStatusCode>() { HttpStatusCode.OK, HttpStatusCode.Accepted, HttpStatusCode.Continue, HttpStatusCode.Redirect, HttpStatusCode.Created, HttpStatusCode.Found };

        public SingleSignOnClient()
        {
            _userAgent = $"EVE.SingleSignOn/{ConfigHelper.GetAssemblyVersion()} Contact/{{0}}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public SsoResponse Authenticate(string code)
        {
            return Authenticate(code, new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public SsoResponse Authenticate(string code, SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException("Authentication code is null or empty.");

            UriBuilder requestUrl = new UriBuilder(settings.BaseUrl) { Path = "oauth/token" };
            RestRequest request = new RestRequest();

            // Headers
            request.AddHeader("User-Agent", string.Format(_userAgent, settings.ContactEmail));
            request.AddHeader("Authorization", TokenType.Basic + " " + AuthorizationString(settings));
            request.AddHeader("Host", new Uri(settings.BaseUrl).Host);

            // Parameters
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("code", code);

            // Settings to make sure we receive a JSON response
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/x-www-form-urlencoded+json"; };
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;

            RestClient client = new RestClient(requestUrl.Uri);

            IRestResponse<SsoResponse> response = client.Execute<SsoResponse>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception("An error occurred during the authentication process.", response.ErrorException);
            }
            else if (!_statusCodes.Contains(response.StatusCode))
            {
                throw new HttpException($"The requested URL ({requestUrl}) returned a HTTP status code of {response.StatusCode}");
            }

            return JsonConvert.DeserializeObject<SsoResponse>(response.Content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<SsoResponse> AuthenticateAsync(string code)
        {
            return await AuthenticateAsync(code, new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public async Task<SsoResponse> AuthenticateAsync(string code, SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException("Authentication code is null or empty.");

            UriBuilder requestUrl = new UriBuilder(settings.BaseUrl) { Path = "oauth/token" };
            RestRequest request = new RestRequest();

            // Headers
            request.AddHeader("User-Agent", string.Format(_userAgent, settings.ContactEmail));
            request.AddHeader("Authorization", TokenType.Basic + " " + AuthorizationString(settings));
            request.AddHeader("Host", new Uri(settings.BaseUrl).Host);

            // Parameters
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("code", code);

            // Settings to make sure we receive a JSON response
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/x-www-form-urlencoded+json"; };
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;

            RestClient client = new RestClient(requestUrl.Uri);

            IRestResponse<SsoResponse> response = await client.ExecuteTaskAsync<SsoResponse>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception("An error occurred during the authentication process.", response.ErrorException);
            }
            else if (!_statusCodes.Contains(response.StatusCode))
            {
                throw new HttpException($"The requested URL ({requestUrl}) returned a HTTP status code of {response.StatusCode}");
            }

            return JsonConvert.DeserializeObject<SsoResponse>(response.Content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetAuthenticationUrl()
        {
            return GetAuthenticationUrl(new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public string GetAuthenticationUrl(SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            UriBuilder uri = new UriBuilder(settings.BaseUrl)
            {
                Path = "oauth/authorize",
                Query = $"response_type=code&redirect_uri={settings.CallbackUrl}&client_id={settings.ClientId}&scope={settings.Scope}&state={settings.State}"
            };

            return uri.Uri.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SsoCharacter GetCharacter()
        {
            if (IsLoggedIn())
                return HttpContext.Current.Session[_characterKey] as SsoCharacter;

            return default(SsoCharacter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsLoggedIn()
        {
            return HttpContext.Current.Session[_characterKey] != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        public SsoResponse Refresh(string refreshToken)
        {
            return Refresh(refreshToken, new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public SsoResponse Refresh(string refreshToken, SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            if (string.IsNullOrEmpty(refreshToken))
                throw new ArgumentNullException("Refresh token is null or empty.");

            UriBuilder requestUrl = new UriBuilder(settings.BaseUrl) { Path = "oauth/token" };
            RestRequest request = new RestRequest();

            // Headers
            request.AddHeader("User-Agent", string.Format(_userAgent, settings.ContactEmail));
            request.AddHeader("Authorization", TokenType.Basic + " " + AuthorizationString(settings));
            request.AddHeader("Host", new Uri(settings.BaseUrl).Host);

            // Parameters
            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("refresh_token", refreshToken);

            // Settings to make sure we receive a JSON response
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/x-www-form-urlencoded+json"; };
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;

            RestClient client = new RestClient(requestUrl.Uri);

            IRestResponse<SsoResponse> response = client.Execute<SsoResponse>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception("An error occurred during the authentication process.", response.ErrorException);
            }
            else if (!_statusCodes.Contains(response.StatusCode))
            {
                throw new HttpException($"The requested URL ({requestUrl}) returned a HTTP status code of {response.StatusCode}");
            }

            return JsonConvert.DeserializeObject<SsoResponse>(response.Content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        public async Task<SsoResponse> RefreshAsync(string refreshToken)
        {
            return await RefreshAsync(refreshToken, new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public async Task<SsoResponse> RefreshAsync(string refreshToken, SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            if (string.IsNullOrEmpty(refreshToken))
                throw new ArgumentNullException("Refresh token is null or empty.");

            UriBuilder requestUrl = new UriBuilder(settings.BaseUrl) { Path = "oauth/token" };
            RestRequest request = new RestRequest();

            // Headers
            request.AddHeader("User-Agent", string.Format(_userAgent, settings.ContactEmail));
            request.AddHeader("Authorization", TokenType.Basic + " " + AuthorizationString(settings));
            request.AddHeader("Host", new Uri(settings.BaseUrl).Host);

            // Parameters
            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("refresh_token", refreshToken);

            // Settings to make sure we receive a JSON response
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/x-www-form-urlencoded+json"; };
            request.Method = Method.POST;
            request.RequestFormat = DataFormat.Json;

            RestClient client = new RestClient(requestUrl.Uri);

            IRestResponse<SsoResponse> response = await client.ExecuteTaskAsync<SsoResponse>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception("An error occurred during the authentication process.", response.ErrorException);
            }
            else if (!_statusCodes.Contains(response.StatusCode))
            {
                throw new HttpException($"The requested URL ({requestUrl}) returned a HTTP status code of {response.StatusCode}");
            }

            return JsonConvert.DeserializeObject<SsoResponse>(response.Content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public SsoCharacter Verify(string accessToken)
        {
            return Verify(accessToken, new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public SsoCharacter Verify(string accessToken, SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            if (string.IsNullOrEmpty(accessToken))
                throw new ArgumentNullException("Access token is null or empty");

            RestRequest request = new RestRequest();

            // Headers
            request.AddHeader("User-Agent", string.Format(_userAgent, settings.ContactEmail));
            request.AddHeader("Authorization", TokenType.Bearer + " " + accessToken);
            request.AddHeader("Host", new Uri(settings.BaseUrl).Host);

            UriBuilder requestUrl = new UriBuilder(settings.BaseUrl) { Path = "oauth/verify" };
            RestClient client = new RestClient(requestUrl.Uri);

            IRestResponse<SsoCharacter> response = client.Execute<SsoCharacter>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception("An error occurred while trying to verify access token.", response.ErrorException);
            }
            else if (!_statusCodes.Contains(response.StatusCode))
            {
                throw new HttpException($"The requested URL ({requestUrl}) returned a HTTP status code of {response.StatusCode}");
            }

            // Deserialize the character informaiton into an object
            SsoCharacter character = JsonConvert.DeserializeObject<SsoCharacter>(response.Content);

            // Store the character in a session
            HttpContext.Current.Session[_characterKey] = character;

            // Return the character information
            return character;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<SsoCharacter> VerifyAsync(string accessToken)
        {
            return await VerifyAsync(accessToken, new SsoSettings());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public async Task<SsoCharacter> VerifyAsync(string accessToken, SsoSettings settings)
        {
            if (settings == null)
                throw new NullReferenceException($"SSO Settings may not be null in {MethodBase.GetCurrentMethod().Name}");

            if (string.IsNullOrEmpty(accessToken))
                throw new ArgumentNullException("Access token is null or empty");

            UriBuilder requestUrl = new UriBuilder(settings.BaseUrl) { Path = "oauth/verify" };
            RestRequest request = new RestRequest();

            // Headers
            request.AddHeader("User-Agent", string.Format(_userAgent, settings.ContactEmail));
            request.AddHeader("Authorization", TokenType.Bearer + " " + accessToken);
            request.AddHeader("Host", new Uri(settings.BaseUrl).Host);

            RestClient client = new RestClient(requestUrl.Uri);

            IRestResponse<SsoCharacter> response = await client.ExecuteTaskAsync<SsoCharacter>(request);

            if (response.ErrorException != null || !string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception("An error occurred while trying to verify access token.", response.ErrorException);
            }
            else if (!_statusCodes.Contains(response.StatusCode))
            {
                throw new HttpException($"The requested URL ({requestUrl}) returned a HTTP status code of {response.StatusCode}");
            }

            // Deserialize the character informaiton into an object
            SsoCharacter character = JsonConvert.DeserializeObject<SsoCharacter>(response.Content);

            // Store the character in a session
            HttpContext.Current.Session[_characterKey] = character;

            // Return the character information
            return character;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Version()
        {
            return ConfigHelper.GetAssemblyVersion();
        }

        /// <summary>
        /// Authorization string takes the clientId and secret, and encodes it into a Base 64 string.
        /// When sent to the CCP Single Sign On service, it returns an access token, if both values are valid.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        private string AuthorizationString(SsoSettings settings)
        {
            if (settings != null)
                return Convert.ToBase64String(Encoding.UTF8.GetBytes(settings.ClientId + ":" + settings.ClientSecret));

            throw new NullReferenceException($"Single Sign On settings may not be null in {MethodBase.GetCurrentMethod().Name}");
        }

        /// <summary>
        /// Authorization string takes the clientId and secret, and encodes it into a Base 64 string.
        /// When sent to the CCP Single Sign On service, it returns an access token, if both values are valid.
        /// </summary>
        /// <returns></returns>
        private string AuthorizationString()
        {
            return AuthorizationString(new SsoSettings());
        }
    }
}