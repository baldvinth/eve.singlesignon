﻿using System.Threading.Tasks;

namespace EVE.SingleSignOn
{
    public interface ISingleSignOnClient
    {
        /// <summary>
        /// Authenticating the code that we receive from the SSO
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        SsoResponse Authenticate(string code);

        /// <summary>
        /// Authenticating the code that we receive from the SSO
        /// </summary>
        /// <param name="code"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        SsoResponse Authenticate(string code, SsoSettings settings);

        /// <summary>
        /// Authenticating the code that we receive from the SSO
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<SsoResponse> AuthenticateAsync(string code);

        /// <summary>
        /// Authenticating the code that we receive from the SSO
        /// </summary>
        /// <param name="code"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        Task<SsoResponse> AuthenticateAsync(string code, SsoSettings settings);

        /// <summary>
        /// Retrieve the authentication URL for the EVE Online SSO
        /// </summary>
        /// <returns></returns>
        string GetAuthenticationUrl();

        /// <summary>
        /// Retrieve the authentication URL for the EVE Online SSO
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        string GetAuthenticationUrl(SsoSettings settings);

        /// <summary>
        /// Get character information
        /// </summary>
        /// <returns></returns>
        SsoCharacter GetCharacter();

        /// <summary>
        /// Returns a boolean value on whether character information is stored in session
        /// </summary>
        /// <returns></returns>
        bool IsLoggedIn();

        /// <summary>
        /// Use a refresh token to receive a new authentication response
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        SsoResponse Refresh(string refreshToken);

        /// <summary>
        /// Use a refresh token to receive a new authentication response
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        SsoResponse Refresh(string refreshToken, SsoSettings settings);

        /// <summary>
        /// Use a refresh token to receive a new authentication response
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        Task<SsoResponse> RefreshAsync(string refreshToken);

        /// <summary>
        /// Use a refresh token to receive a new authentication response
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        Task<SsoResponse> RefreshAsync(string refreshToken, SsoSettings settings);

        /// <summary>
        /// Verify an access token, receiving a character object from the EVE Online SSO
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        SsoCharacter Verify(string accessToken);

        /// <summary>
        /// Verify an access token, receiving a character object from the EVE Online SSO
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        SsoCharacter Verify(string accessToken, SsoSettings settings);

        /// <summary>
        /// Verify an access token, receiving a character object from the EVE Online SSO
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        Task<SsoCharacter> VerifyAsync(string accessToken);

        /// <summary>
        /// Verify an access token, receiving a character object from the EVE Online SSO
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        Task<SsoCharacter> VerifyAsync(string accessToken, SsoSettings settings);

        /// <summary>
        /// Retrieve the user agent for Single Sign On Client
        /// </summary>
        /// <returns></returns>
        string Version();
    }
}
