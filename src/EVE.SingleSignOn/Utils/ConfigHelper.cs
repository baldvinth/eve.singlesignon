﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using System.Text.RegularExpressions;

namespace EVE.SingleSignOn.Utils
{
    public class ConfigHelper
    {
        /// <summary>
        /// Retrieve application setting in the form of a string.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(value))
                throw new NullReferenceException(string.Format("The value for '{0}' is empty or key is missing from web.config", key));

            return value;
        }

        /// <summary>
        /// Retrieve application setting in the form of a string.
        /// If the string is empty or missing, we return an empty string instead.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSettingNullable(string key)
        {
            string value = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return value;
        }

        /// <summary>
        /// Retrieve an e-mail address from application settings.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAppSettingEmail(string key)
        {
            string value = GetAppSettingNullable(key);

            if (!string.IsNullOrEmpty(value) && !Regex.IsMatch(value, @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$", RegexOptions.IgnoreCase))
                throw new FormatException(string.Format("The value for key '{0}' in web.config is not a valid e-mail address.", key));

            return value;
        }

        /// <summary>
        /// Retrieve application setting in the form of an integer.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetAppSettingInt(string key)
        {
            return int.Parse(GetAppSetting(key));
        }

        /// <summary>
        /// Retrieve application setting in the form of boolean
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetAppSettingBool(string key)
        {
            return bool.Parse(GetAppSetting(key));
        }

        /// <summary>
        /// Get the Assembly file version
        /// </summary>
        /// <returns></returns>
        public static string GetAssemblyVersion()
        {
            return FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
        }
    }
}