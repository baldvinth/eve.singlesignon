﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EVE.SingleSignOn")]
[assembly: AssemblyDescription("Client for use with third party development. The task of this project is to make it easy for anyone to make a .NET application with EVE online logins.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Baldvin Th")]
[assembly: AssemblyProduct("EVE.SingleSignOn")]
[assembly: AssemblyCopyright("Copyright © Baldvin Th 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ced3e311-8d13-4c9a-aaa9-1ad8bb23f9c8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.16231.1234")]
[assembly: AssemblyFileVersion("1.0.16231.1234")]
