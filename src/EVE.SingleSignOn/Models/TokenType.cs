﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EVE.SingleSignOn
{
    public enum TokenType
    {
        Unknown,
        Basic,
        Bearer,
        Character
    }
}